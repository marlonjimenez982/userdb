package com.example.userform;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {
    //paso 7 represemta una tabla en DB
    //colmunas
    @PrimaryKey(autoGenerate = true)
    public int vid;

    @ColumnInfo(name = "nombre")
    public String nombre;

    @ColumnInfo(name = "apellido")
    public String apellido;

    @ColumnInfo(name = "peso")
    public float peso;



}
