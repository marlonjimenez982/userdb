package com.example.userform;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

//Parte 11 crear el D A O con lo que se requiera

@Dao

public interface UserDAO {

    @Insert
    void insertarUsuario(User miUsuario);

    @Query("SELECT * FROM User")
    List<User> obtenerTodos();

    @Query("SELECT * FROM User WHERE vid=:id")
    User obtenerUsuario(int id);

    @Update
    void actualizarUsuario(User miUsuario);
}
