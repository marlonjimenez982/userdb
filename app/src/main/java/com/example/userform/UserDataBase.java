package com.example.userform;

import android.content.Context;

import androidx.room.Database;
import androidx.room.RoomDatabase;


//representar una conexion paso 12

@Database(entities = {User.class}, version = 1)
//Si se cambian las entidades (User) cambias la version


public abstract class UserDataBase extends RoomDatabase {

    private static volatile UserDataBase INSTANCE;
    public static UserDataBase getDatabase(Context ctx){

        //conectarme a la DB
        INSTANCE= androidx.room.Room.databaseBuilder(ctx.getApplicationContext(), UserDataBase.class,"usuarios")
                .build();
        return INSTANCE;
    }

    //Declarar los DAO
    public abstract UserDAO userDao();

}
