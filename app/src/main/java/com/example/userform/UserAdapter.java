package com.example.userform;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter <UserView>{
    //paso 5 adaptador o robot que mueve las cosas ojo implemetar los tres metodos siguientes con el bombillo rojo

    private List<User> users;

    //paso 8:
    //Constructor = recibe los datos que debe de mostrar
    public UserAdapter(List<User> users){

        this.users=users;
    }
//paso 8

    @NonNull
    @Override
    public UserView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Return; ya no se requiere

        //paso 8 crear el contexto y cajon
        //al crear un paquete (vista) donde voy a guardar un dato
        Context ctx=parent.getContext();
        LayoutInflater creadordevistas= LayoutInflater.from(ctx);
        View cajondeusuario= creadordevistas.inflate(R.layout.userview,parent,false);
        UserView uncajon=new UserView(cajondeusuario);
        return uncajon;
    }

    @Override
    public void onBindViewHolder(@NonNull UserView holder, int position) {
        //paso 9 enlazar los datos de los usuarios al textview
        //Guardar un dato en el paquete de onCreate

        User miUsuario=users.get(position); //obtener un usuario de la lista
        //asociar y nombrar
        TextView dato=holder.nombre;
        dato.setText(miUsuario.nombre);
    }

    //Dice cuantos elementos recibio para mostrar en la pantalla
    @Override
    public int getItemCount() {
//        return 0; no se va a usar
        //paso 10 contar usuarios
        if (users==null){
            return 0;
        }
        return users.size();

    }

}
