package com.example.userform;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class DBList extends AppCompatActivity {
    //paso 2 cear la actividad con el recycler view con clic derecho en java de project

    RecyclerView miVistaReciclador;
    UserDataBase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dblist);

        //paso 13 despues del DAO y UserDataBase
        miVistaReciclador =findViewById(R.id.listaUsuarios);
        db= UserDataBase.getDatabase(this);

        //Cargar los datos  empleando el DAO
        Bundle misDatos=getIntent().getExtras();
        User miUsuario=new User();
        miUsuario.nombre=misDatos.getString("nombre");
        miUsuario.apellido=misDatos.getString("apellido");
        miUsuario.peso=Float.parseFloat(misDatos.getString("peso"));

        //paso 14
        //cargar DAO
        //crear un nuevo hilo
        new Thread(
                ()->{
                    db.userDao().insertarUsuario(miUsuario);
                }
        ).start();



    }
}